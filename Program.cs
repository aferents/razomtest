﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;

namespace RazomTestApp
{
    class Program
    {
        public static IConfigurationRoot configuration;

        static int Main(string[] args)
        {
            // Initialize serilog logger
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console(Serilog.Events.LogEventLevel.Debug)
                .MinimumLevel.Debug()
                .Enrich.FromLogContext()
                .CreateLogger();

            try
            {
                Execute();
                return 0;
            }
            catch (Exception e)
            {
                Log.Error(e, "App finished with Error");
                return 1;
            }
        }

        static void Execute()
        {
            ServiceCollection serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);

            // Create service provider
            serviceCollection.BuildServiceProvider();

            Log.Information($"Reading files from source folder:{configuration["SourceDirectory"]}");
            var filePaths = Directory.GetFiles(configuration["SourceDirectory"], "*.html");

            foreach (var filePath in filePaths)
            {
                var fileName = Path.GetFileName(filePath);
                var outputPath = Path.Combine(configuration["OutputDirectory"], fileName);

                HtmlDocument document = new HtmlDocument();
                
                document.Load(File.OpenRead(filePath));

                foreach (HtmlNode node in document.DocumentNode.SelectNodes("//a[@href]"))
                {
                    HtmlAttribute att = node.Attributes["href"];

                    if (att.Value.Equals(node.InnerText)) continue;

                    // in case any formatiing present in string, so need to be cleaned
                    var fixedText = Regex.Replace(node.InnerText, @"\s+", "");

                    att.Value = fixedText;
                }

                Log.Information($"Writting file:{fileName} to the output folder: {configuration["OutputDirectory"]}");
                FileStream streamWriter = new FileStream(outputPath, FileMode.Create);

                document.Save(streamWriter);

                streamWriter.Close();
            }

            Log.Information($"App finished execution successfully");

        }

        private static void ConfigureServices(IServiceCollection serviceCollection)
        {
            // Add logging
            serviceCollection.AddSingleton(LoggerFactory.Create(builder =>
            {
                builder
                    .AddSerilog(dispose: true);
            }));

            serviceCollection.AddLogging();

            // Build configuration
            configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(AppContext.BaseDirectory).FullName)
                .AddJsonFile("appsettings.json", false)
                .Build();

            // Add access to generic IConfigurationRoot
            serviceCollection.AddSingleton<IConfigurationRoot>(configuration);

        }
    }
}
